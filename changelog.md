# 0.5.0.0 (December 5, 2017)

* bump ynot-prelude version: 0.1.8.0 -> 0.2.0.0
* rm doctests, hlint
* rm comment headers
* add GHC warn all to all source files
* lts: 8.6 -> 9.17

# 0.4.4.0 (April 27, 2017)

* bump ynot-prelude version: 0.1.7.0 -> 0.1.8.0

# 0.4.3.0 (April 27, 2017)

* bump ynot-prelude version

# 0.4.2.0 (April 23, 2017)

* bump ynot-prelude version

# 0.4.1.1 (April 23, 2017)

* bump ynot-prelude version

# 0.4.1.0 (April 23, 2017)

* bump ynot-prelude version

# 0.4.0.0 (April 8, 2017)

* significantly expanded default extensions:

```haskell
  default-extensions:
                    -- because it's really important
                     NoImplicitPrelude

                     -- syntax
                   , BinaryLiterals
                   , LambdaCase
                   , TupleSections
                   , NegativeLiterals
                   , OverloadedStrings

                   -- operational extensions/changes
                   , ApplicativeDo
                   , Arrows
                   , BangPatterns
                   , MagicHash
                   , UnboxedTuples

                   -- deriving extensions
                   , DeriveFoldable
                   , DeriveFunctor
                   , DeriveTraversable
                   , GeneralizedNewtypeDeriving
                   , StandaloneDeriving

                   -- record extensions
                   , DisambiguateRecordFields
                   , DuplicateRecordFields
                   , NamedFieldPuns
                   , NamedWildCards
                   , RecordWildCards
                   , OverloadedLabels

                   -- type class extensions
                   , FlexibleInstances
                   , FunctionalDependencies
                   , MultiParamTypeClasses

                   -- dependent types/general type-system enhancements
                   , ConstraintKinds
                   , DataKinds
                   , EmptyCase
                   , EmptyDataDecls
                   , GADTs
                   , KindSignatures
                   , PartialTypeSignatures
                   , RankNTypes
                   , RoleAnnotations
                   , ScopedTypeVariables
                   , TypeApplications
                   , TypeFamilies
                   , TypeFamilyDependencies
                   , TypeOperators
                   , TypeSynonymInstances
```

# 0.3.1.1 (March 29, 2017)

* improved default date for changelog
* resolver: lts-8.5 -> lts-8.6

# 0.3.1.0 (March 15, 2017)

* Clarify contributions and limitations of support
* resolver default: 7.15 -> 8.5
* Better readme

# 0.3.0.0 (January 9, 2017)

* Working full project template
    * doctests
    * hlint
    * hspec & property tests
    * benchmarks
* added `resolver` variable

# 0.2.0.0 (January 8, 2017)

* Fixed README table header

# 0.1.0.0 (January 8, 2017)

* Initial
