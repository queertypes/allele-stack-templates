# Allele's Haskell Stack Templates

| Allele's Stack Templates | 0.5.0.0                           |
| ------------------------ | --------------------------------- |
| Maintainer               | Allele Dev (allele.dev@gmail.com) |
| Funding                  | $0 USD                            |
| Copyright                | Copyright (C) 2017 Allele Dev     |
| License                  | GPL-3                             |



For use with haskell [stack](https://docs.haskellstack.org/en/stable/README/)

Just a set of generators to get initial project setup out of the way.

## GPL-3 Licensed Full Project, YNotPrelude

* NoImplicitPrelude
    * Uses [YNotPrelude](https://gitlab.com/queertypes/ynot-prelude)
* several language extensions enabled by default
* Doctest support
* HLint support
* Tasty for tests, with quickcheck and hspec
* Criterion benchmarks
* GPL-3 license
* Contributor's covenant code of conduct
* README structure with some sections filled in
* basic changelog
* .gitignore
* comment header on all source files
* .cabal-project
